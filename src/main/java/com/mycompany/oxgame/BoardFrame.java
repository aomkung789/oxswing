/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class BoardFrame extends javax.swing.JFrame {

    private int row;
    private int col;
    JButton btnTables[][] = null;
    Table table;
    Player o;
    Player x;

    /**
     * Creates new form BoardFrame
     */
    public BoardFrame() {
        initComponents();
        initTable();
        initGame();
        showTable();
        showTurn();
        hideNewGame();
        btnNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newGame();
            }
        });

    }

    public void initGame() {
        o = new Player('O');
        x = new Player('X');
        table = new Table(o, x);
    }

    public void initTable() {
        JButton tables[][] = {{btnTable1, btnTable2, btnTable3},
        {btnTable4, btnTable5, btnTable6},
        {btnTable7, btnTable8, btnTable9}};
        btnTables = tables;
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables.length; j++) {
                btnTables[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String strCom = e.getActionCommand();
                        String coms[] = strCom.split(",");
                        row = Integer.parseInt(coms[0]);
                        col = Integer.parseInt(coms[1]);

                        if (table.setRowCol(row, col)) {
                            showTable();
                            if (table.checkWin()) {
                                showWin();
                                showStatXO();
                                return;
                            }
                            switchTurn();

                        }

                    }

                });
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnTable1 = new javax.swing.JButton();
        btnTable2 = new javax.swing.JButton();
        btnTable3 = new javax.swing.JButton();
        btnTable4 = new javax.swing.JButton();
        btnTable5 = new javax.swing.JButton();
        btnTable6 = new javax.swing.JButton();
        btnTable7 = new javax.swing.JButton();
        btnTable8 = new javax.swing.JButton();
        btnTable9 = new javax.swing.JButton();
        showRowCol = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        opJX = new javax.swing.JPanel();
        scoreX = new javax.swing.JLabel();
        opJO = new javax.swing.JPanel();
        scoreO = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTable1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable1.setText("-");
        btnTable1.setActionCommand("1,1");
        btnTable1.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable1.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable1.setPreferredSize(new java.awt.Dimension(80, 80));
        btnTable1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable1ActionPerformed(evt);
            }
        });

        btnTable2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable2.setText("-");
        btnTable2.setActionCommand("1,2");
        btnTable2.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable2.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable2.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable3.setText("-");
        btnTable3.setActionCommand("1,3");
        btnTable3.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable3.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable3.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable4.setText("-");
        btnTable4.setActionCommand("2,1");
        btnTable4.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable4.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable4.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable5.setText("-");
        btnTable5.setActionCommand("2,2");
        btnTable5.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable5.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable5.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable6.setText("-");
        btnTable6.setActionCommand("2,3");
        btnTable6.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable6.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable6.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable7.setText("-");
        btnTable7.setActionCommand("3,1");
        btnTable7.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable7.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable7.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable8.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable8.setText("-");
        btnTable8.setActionCommand("3,2");
        btnTable8.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable8.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable8.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable9.setText("-");
        btnTable9.setActionCommand("3,3");
        btnTable9.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable9.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable9.setPreferredSize(new java.awt.Dimension(80, 80));

        showRowCol.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        showRowCol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        showRowCol.setText("ROW: ?, COL:?");

        btnNewGame.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnNewGame.setText("New Game");
        btnNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGameActionPerformed(evt);
            }
        });

        opJX.setBackground(new java.awt.Color(186, 255, 201));
        opJX.setName(""); // NOI18N

        scoreX.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        scoreX.setText("X -> Win: 0 , Lose: 0 , Draw: 0");

        javax.swing.GroupLayout opJXLayout = new javax.swing.GroupLayout(opJX);
        opJX.setLayout(opJXLayout);
        opJXLayout.setHorizontalGroup(
            opJXLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(opJXLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(scoreX, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        opJXLayout.setVerticalGroup(
            opJXLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(opJXLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(scoreX, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        opJO.setBackground(new java.awt.Color(186, 225, 255));

        scoreO.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        scoreO.setText("O -> Win: 0 , Lose: 0 , Draw: 0");

        javax.swing.GroupLayout opJOLayout = new javax.swing.GroupLayout(opJO);
        opJO.setLayout(opJOLayout);
        opJOLayout.setHorizontalGroup(
            opJOLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(opJOLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(scoreO, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        opJOLayout.setVerticalGroup(
            opJOLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(opJOLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(scoreO, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(showRowCol, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opJX, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(opJO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addComponent(btnNewGame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(opJX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(opJO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(showRowCol, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTable1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnTable1ActionPerformed

    private void btnNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNewGameActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BoardFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnTable1;
    private javax.swing.JButton btnTable2;
    private javax.swing.JButton btnTable3;
    private javax.swing.JButton btnTable4;
    private javax.swing.JButton btnTable5;
    private javax.swing.JButton btnTable6;
    private javax.swing.JButton btnTable7;
    private javax.swing.JButton btnTable8;
    private javax.swing.JButton btnTable9;
    private javax.swing.JPanel opJO;
    private javax.swing.JPanel opJX;
    private javax.swing.JLabel scoreO;
    private javax.swing.JLabel scoreX;
    private javax.swing.JLabel showRowCol;
    // End of variables declaration//GEN-END:variables

    private void hideNewGame() {
        btnNewGame.setVisible(false);
    }

    private void showNewGame() {
        btnNewGame.setVisible(true);
    }

    private void showTable() {
        char data[][] = table.getData();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables.length; j++) {
                btnTables[i][j].setText("" + data[i][j]);
            }
        }

    }

    private void switchTurn() {
        table.switchPlayer();
        showTurn();
    }

    public void showTurn() {
        showRowCol.setText("Turn: " + table.getCurrentPlayer().getName());
    }

    public void showWin() {
        if (table.getWinner() == null) {
            showRowCol.setText("Drawwww!!");

        } else {
            showRowCol.setText(table.getWinner().getName() + " Win!!!");

        }

        char data[][] = table.getData();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables.length; j++) {
                btnTables[i][j].setEnabled(false);
            }
        }

        showNewGame();
    }

    public void newGame() {
        table = new Table(o, x);
        showTable();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables.length; j++) {
                btnTables[i][j].setEnabled(true);
            }
        }
    }

    public void showStatXO() {
        scoreX.setText("X -> Win: " + x.getWin() + " , Lose: " + x.getLose() + " , Draw: " + x.getDraw());
        scoreO.setText("O -> Win: " + o.getWin() + " , Lose: " + o.getLose() + " , Draw: " + o.getDraw());
    }

}
